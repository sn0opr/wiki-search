## Installation
After cloning the reposetory, please run the command line 
```composer install --no-dev```

## Usage
Go to the project path and run ```php artisan wiki-search --keyword= --sort=```

## Code description

I made a command line class ```C:\wamp\www\wiki-search\app\Console\Commands\WikiSearch.php```

I also made a service class so it can be resued ```C:\wamp\www\wiki-search\app\Services\WikiSearch.php```

I used Htmldom (https://github.com/yangqi/Htmldom) package for loading and parsing HTML.

## wiki-search command line
The command line tool will support the following functionality:
General
All lists will be output as a two column table using the standard console table library of your chosen framework. 
The output will look similar (although not exactly the same) to the attached image "CommandLineTableExample.png". 
The two columns will be "Title", and "Link" e.g displaying data "Algebraic signal processing", "https://en.wikipedia.org/wiki/Algebraic_signal_processing"
Commands
wiki-search --sort=$sort : $sort can be ASC or DESC and will output all the links in the table sorted alphabetically according to the ASC or DESC value.
wiki-search --search=$keyword : This will only output links who's title or URL have a case insensitive levenshtein distance of less than 3 or 0 from the $keyword e.g wiki-search --search=lemma
wiki-search : If the command is run with no arguments, all 122 links are output, orded alphabetically ascening. wiki-search is equivalent to wiki-search --sort=ASC
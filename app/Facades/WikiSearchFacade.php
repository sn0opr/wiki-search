<?php

/**
 * Created by PhpStorm.
 * User: Sn0opr
 * Date: 20/09/2015
 * Time: 18:30
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class WikiSearchFacade extends Facade
{

    protected static function getFacadeAccessor() { return 'App\Services\WikiSearch'; }

}
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class WikiSearch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wiki-search
                            {--keyword= : Search by keyword}
                            {--sort= : sorting by "desc" or "asc"}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Outputs urls from https://en.wikipedia.org/wiki/Category:Algebra';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Please wait, it\'s loading...');
        $headers = ['Title', 'Link'];
        $links = \WikiSearch::search($this->option('keyword'), $this->option('sort'));
        $this->table($headers, $links->toArray());
    }
}

<?php

namespace App\Providers;

use App\Services\WikiSearch;
use Illuminate\Support\ServiceProvider;
use Yangqi\Htmldom\Htmldom;

class WikiSearchServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Services\WikiSearch::class, function ($app) {
            return new WikiSearch(new Htmldom());
        });
    }
}
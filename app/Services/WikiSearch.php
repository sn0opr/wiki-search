<?php
/**
 * Created by PhpStorm.
 * User: Sn0opr
 * Date: 20/09/2015
 * Time: 18:16
 */

namespace App\Services;


use Illuminate\Support\Collection;
use Yangqi\Htmldom\Htmldom;

class WikiSearch
{

    private $htmldom;

    const PAGE_URL = 'https://en.wikipedia.org/wiki/Category:Algebra';

    public function __construct(Htmldom $htmldom) {
        $this->htmldom = $htmldom;
    }


    public function search($keyword="", $sort = "") {

        $this->htmldom->load_file(self::PAGE_URL);

        if (!empty($sort) && $sort != 'desc' && $sort != 'asc') {
            throw new \InvalidArgumentException('Sort value invalide');
        }

        $links = $this->htmldom->find('#mw-pages > .mw-category-group > a');

        $pagesCollection = new Collection();

        foreach ($links as $link) {
            $pagesCollection->push([
                'title' => $link->innertext,
                'link' => 'https://en.wikipedia.org/wiki'.$link->href
            ]);
        }

        if ($keyword) {
            $pagesCollection = $pagesCollection->filter(function($item) use($keyword) {
                return levenshtein(strtolower($item['title']), strtolower($keyword)) <= 3 || levenshtein(strtolower($item['link']), strtolower($keyword)) <= 3;
            });
        }

        if ($sort) {
            $pagesCollection = $pagesCollection->sortBy('title', null, $sort == 'desc');
        }
        return $pagesCollection;
    }
}